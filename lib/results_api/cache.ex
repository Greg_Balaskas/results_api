defmodule ResultsAPI.Cache do
  @moduledoc """
  Module responsible for creating and accesing the application cache.
  The cache is consisted of one ETS table for every division (seasons as keys).
  The reference of those ETS tables are held in another ETS table (divisions as keys),
  that serves as a registry. Read concurrency is enable in all tables,
  so the simultaneous access from multiple processes is faster.
  """

  alias NimbleCSV.RFC4180, as: CSV
  alias ResultsAPI.Models.Match

  @doc """
  Function that reads a csv file, parses it into modeled structs and inserts it into the cache.
  """
  @spec create_from_csv(String.t()) :: :ok
  def create_from_csv(csv_path) do
    ets = :ets.new(:cache, [:named_table, :set, :protected, read_concurrency: true])

    csv_path
    |> File.stream!()
    |> CSV.parse_stream()
    |> Stream.map(&insert_row_in_ets(&1, ets))
    |> Stream.run()
  end

  @doc """
  Function that gets the keys (season) from the ETS tables
  and returns them paired with the divisions.
  """
  @spec get_div_season_pairs :: [map]
  def get_div_season_pairs() do
    divs = :ets.match(:cache, {:"$1", :"$2"})

    Enum.reduce(divs, [], fn [div, div_ets], acc -> create_div_season_pairs(div, div_ets, acc) end)
  end

  @doc """
  Function that returns a list of the Result models stored in the cache,
  given the division and the season in the form of atom and integer.
  In case no results are found in the cache, an error tuple is returned.
  """
  @spec get_results_for_div_season(atom, integer) ::
          {:ok, [Match.t()]} | {:error, :div_not_in_data | :no_results_for_div_season_pair}
  def get_results_for_div_season(div, season) do
    case :ets.lookup(:cache, div) do
      [] -> {:error, :div_not_in_data}
      [{_, div_ets}] -> get_results_for_season(div_ets, season)
    end
  end

  defp insert_row_in_ets(row, ets) do
    row
    |> Match.parse_row()
    |> insert_result_in_ets(ets)
  end

  defp insert_result_in_ets(%Match{div: div, season: season} = result, ets) do
    case :ets.lookup(ets, div) do
      [] ->
        div_ets = :ets.new(div, [:bag, :protected, read_concurrency: true])
        :ets.insert(div_ets, {season, result})
        :ets.insert(ets, {div, div_ets})

      [{_div, div_ets}] ->
        :ets.insert(div_ets, {season, result})
    end
  end

  defp create_div_season_pairs(div, div_ets, acc) do
    seasons = :ets.match(div_ets, {:"$1", :_})

    seasons
    |> Enum.map(fn [season] -> %{div: div, season: season} end)
    |> Enum.uniq()
    |> Enum.reduce(acc, fn pair, acc -> [pair | acc] end)
  end

  defp get_results_for_season(div_ets, season) do
    # empty lookup result returns an error so it can be easily handled differently
    case :ets.lookup(div_ets, season) do
      [] -> {:error, :no_results_for_div_season_pair}
      results -> {:ok, Enum.map(results, fn {_, result} -> result end)}
    end
  end
end
