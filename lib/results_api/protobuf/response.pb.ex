defmodule ResultsAPI.Protobuf.Response do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          div: String.t(),
          season: String.t(),
          matches: [ResultsAPI.Protobuf.Match.t()]
        }
  defstruct [:div, :season, :matches]

  field :div, 1, type: :string
  field :season, 2, type: :string
  field :matches, 3, repeated: true, type: ResultsAPI.Protobuf.Match
end

defmodule ResultsAPI.Protobuf.Match do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          date: String.t(),
          pair: ResultsAPI.Protobuf.Pair.t() | nil,
          ft_result: ResultsAPI.Protobuf.Result.t() | nil,
          ht_result: ResultsAPI.Protobuf.Result.t() | nil
        }
  defstruct [:date, :pair, :ft_result, :ht_result]

  field :date, 1, type: :string
  field :pair, 2, type: ResultsAPI.Protobuf.Pair
  field :ft_result, 3, type: ResultsAPI.Protobuf.Result
  field :ht_result, 4, type: ResultsAPI.Protobuf.Result
end

defmodule ResultsAPI.Protobuf.Pair do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          home_team: String.t(),
          away_team: String.t()
        }
  defstruct [:home_team, :away_team]

  field :home_team, 1, type: :string
  field :away_team, 2, type: :string
end

defmodule ResultsAPI.Protobuf.Result do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          home_goals: integer,
          away_goals: integer,
          result: String.t()
        }
  defstruct [:home_goals, :away_goals, :result]

  field :home_goals, 1, type: :int32
  field :away_goals, 2, type: :int32
  field :result, 3, type: :string
end
