defmodule ResultsAPI.Models.Match do
  @moduledoc """
  Model Struct for representing the whole match information given in the csv dataset.
  """

  alias __MODULE__
  alias ResultsAPI.Models.{Pair, Result}

  @keys [
    :div,
    :season,
    :date,
    :pair,
    :ft_result,
    :ht_result
  ]
  @enforce_keys @keys

  defstruct @keys

  @type t :: %Match{
          div: atom,
          season: non_neg_integer,
          date: String.t(),
          pair: Pair.t(),
          ft_result: Result.t(),
          ht_result: Result.t()
        }

  @doc """
  Function that parses the list of strings, corresponding to the csv table columns,
  into the modelled structure.
  """
  @spec parse_row(list[String.t()]) :: Match.t()
  def parse_row([_, div, season, date, home_team, away_team, fthg, ftag, ftr, hthg, htag, htr]) do
    div = String.to_atom(div)
    season = String.to_integer(season)
    fthg = String.to_integer(fthg)
    ftag = String.to_integer(ftag)
    hthg = String.to_integer(hthg)
    htag = String.to_integer(htag)

    pair = %Pair{home_team: home_team, away_team: away_team}
    ft_result = %Result{home_goals: fthg, away_goals: ftag, result: ftr}
    ht_result = %Result{home_goals: hthg, away_goals: htag, result: htr}

    %Match{
      div: div,
      season: season,
      date: date,
      pair: pair,
      ft_result: ft_result,
      ht_result: ht_result
    }
  end
end
