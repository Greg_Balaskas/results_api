defmodule ResultsAPI.Models.Pair do
  @moduledoc """
  Model for representing a pair of opponent teams.
  """

  alias __MODULE__

  @keys [:home_team, :away_team]
  @enforce_keys @keys
  defstruct @keys

  @type t :: %Pair{home_team: String.t(), away_team: String.t()}
end
