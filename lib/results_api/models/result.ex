defmodule ResultsAPI.Models.Result do
  @moduledoc """
  Model for representing a match result (can be half time or full time).
  """
  alias __MODULE__

  @keys [:home_goals, :away_goals, :result]
  @enforce_keys @keys
  defstruct @keys

  @type t :: %Result{home_goals: non_neg_integer, away_goals: non_neg_integer, result: String.t()}
end
