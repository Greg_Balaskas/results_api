defmodule ResultsAPI.Router do
  @moduledoc """
  Defines a router plug, which also defines the application API.

  The defined endpoints are:
      - 'GET /div_season_pairs'
      - 'GET /results?div=SP1&season=201617'

  The content-types available are application/json and application/protobuf

  The app read the client's accepted content-type and serves the data in this type.

  ## Return codes
  - 200 - path and/or parametres are valid so results are returned
  - 400 - bad arguments given
  - 404 - trying to access wrong path
  - 406 - when unsupported content-type is asked by the client
  - 500 - any unhandled error raised during serving the request
  """

  use Plug.Router
  use Plug.ErrorHandler

  alias ResultsAPI.{Cache, Encoder}

  require Logger

  plug(Plug.Logger)
  plug(:match)
  plug(:dispatch)

  get "/div_season_pairs" do
    pairs = Cache.get_div_season_pairs()
    body = Poison.encode!(pairs)

    conn
    |> resp(200, body)
    |> send_resp()
  end

  get "/results" do
    conn
    |> fetch_query_params()
    |> validate_and_query_cache_and_encode
    |> send_resp()
  end

  match _ do
    Logger.error("Request for a bad resource")
    send_resp(conn, 404, "Resource not found")
  end

  def handle_errors(conn, %{kind: _kind, reason: reason, stack: _stack}) do
    Logger.error("Internal error reason: #{inspect(reason)}")
    send_resp(conn, conn.status, "Something went wrong: #{inspect(reason)}")
  end

  defp validate_and_query_cache_and_encode(conn) do
    with :ok <- validate_query_params(conn),
         {:ok, div, season} <- parse_query_params(conn),
         {:ok, new_conn, format} <- validate_content_type(conn),
         {:ok, results} <- Cache.get_results_for_div_season(div, season) do
      body = Encoder.encode!(format, results, div, season)
      resp(new_conn, 200, body)
    else
      # empty results list is treated as a badarg error
      {:error, :unsupported_content_type} ->
        Logger.error("Requested unsupported content-type")
        resp(conn, 406, "Not supported requested content-type")

      {:error, error} ->
        Logger.error("Error reason: #{error}")
        resp(conn, 400, "Error reason: #{error}")
    end
  end

  defp validate_query_params(conn) do
    # checking if any needed param is absent or has an empty value
    case conn.query_params do
      %{"div" => "", "season" => _season} ->
        {:error, :wrong_div_param}

      %{"div" => _div, "season" => ""} ->
        {:error, :wrong_season_param}

      %{"div" => _div, "season" => season} ->
        validate_season_format(season)

      _ ->
        {:error, :not_enough_params}
    end
  end

  defp validate_content_type(conn) do
    # gets the accept header from the request and puts it in the response content-type
    # also encodes the results accordingly
    case get_req_header(conn, "accept") do
      ["application/json"] ->
        {:ok, put_resp_content_type(conn, "application/json"), :json}

      ["application/protobuf"] ->
        {:ok, put_resp_content_type(conn, "application/protobuf"), :proto}

      _ ->
        {:error, :unsupported_content_type}
    end
  end

  defp validate_season_format(season) do
    case Integer.parse(season) do
      # chechking if season is a 6 digit integer
      {season, _} when is_integer(season) and season > 99_999 -> :ok
      :error -> {:error, :wrong_season_param}
      _ -> {:error, :wrong_season_param}
    end
  end

  defp parse_query_params(conn) do
    # converting div and season in the form they are used internally
    %{"div" => div, "season" => season} = conn.query_params
    {:ok, String.to_atom(div), String.to_integer(season)}
  end
end
