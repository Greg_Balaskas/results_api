defmodule ResultsAPI.Config do
  @moduledoc """
  Defines getter functions for configuration params.
  """

  @app Mix.Project.config()[:app]

  @spec get_csv_path :: String.t() | nil
  def get_csv_path(), do: Application.get_env(@app, :csv_path)

  @spec get_server_config(atom) :: keyword
  def get_server_config(server), do: Application.get_env(@app, server)
end
