defmodule ResultsAPI.Encoder do
  @moduledoc """
  Module responsible for converting the results to the protobuf binary.
  It uses the protobuf messages defined in the response.proto file.
  """

  alias ResultsAPI.Protobuf.{Match, Pair, Response, Result}

  @spec encode!(:json | :proto, [ResultsAPI.Models.Match.t()], atom, integer) :: String.t()
  def encode!(:json, matches, div, season) do
    response = %{div: div, season: season, results: matches}
    Poison.encode!(response)
  end

  def encode!(:proto, matches, div, season) do
    matches = Enum.map(matches, &encode_match/1)
    response = Response.new(div: to_string(div), season: to_string(season), matches: matches)
    Response.encode(response)
  end

  defp encode_match(%{date: date, pair: pair, ft_result: ft_result, ht_result: ht_result}) do
    pair = pair |> Map.from_struct() |> Pair.new()
    ft_result = ft_result |> Map.from_struct() |> Result.new()
    ht_result = ht_result |> Map.from_struct() |> Result.new()
    Match.new(date: date, pair: pair, ft_result: ft_result, ht_result: ht_result)
  end
end
