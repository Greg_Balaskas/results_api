defmodule ResultsAPI.Application do
  @moduledoc """
  ResultsAPI Application module that initializes and observes the cache.
  If Cache (ETS table) crashes, which means something bad in the erlang VM happened,
  the application will crash too.
  """

  use Application

  alias ResultsAPI.{Cache, Config, Router}

  require Logger

  def start(_type, _args) do
    csv_path = Config.get_csv_path()
    Cache.create_from_csv(csv_path)

    Logger.info("Cache initialized")

    server_opts = Config.get_server_config(Router)

    children = [
      Plug.Cowboy.child_spec(scheme: :http, plug: Router, opts: server_opts)
    ]

    opts = [strategy: :one_for_one, name: ResultsAPI.Supervisor]

    Logger.info("Server is initializing...")
    Supervisor.start_link(children, opts)
  end
end
