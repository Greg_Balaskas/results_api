FROM elixir:1.9.1

ARG MIX_ENV
ENV MIX_ENV=${MIX_ENV}

EXPOSE ${PORT}

COPY . /results_api
WORKDIR /results_api

RUN apt-get update
RUN apt-get install git -y
RUN mix local.rebar --force
RUN mix local.hex --force
RUN mix deps.get --only MIX_ENV
RUN mix deps.compile
RUN mix release --path ../release

ENTRYPOINT [ "/release/bin/results_api" ]
CMD ["start"]