defmodule ResultsAPI.MixProject do
  use Mix.Project

  def project do
    [
      app: :results_api,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      elixirc_paths: elixirc_paths(Mix.env()),
      deps: deps()
    ]
  end

  def elixirc_paths(:test), do: ["lib", "test/extras"]
  def elixirc_paths(_), do: ["lib"]

  def application do
    [
      extra_applications: [:logger],
      mod: {ResultsAPI.Application, []}
    ]
  end

  defp deps do
    [
      {:ex_doc, "~> 0.21.2", only: [:dev]},
      {:credo, "~> 1.1", only: [:dev]},
      {:dialyxir, "~> 0.5.1", only: [:dev]},
      {:nimble_csv, "~> 0.6.0"},
      {:poison, "~> 4.0"},
      {:plug_cowboy, "~> 2.1"},
      {:protobuf, "~> 0.6.3"}
    ]
  end
end
