import Config

config :results_api, csv_path: System.get_env("CSV_PATH")

config :results_api, ResultsAPI.Router, port: System.get_env("PORT")
