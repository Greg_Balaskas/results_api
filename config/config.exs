import Config

app = Mix.Project.config()[:app]

config app, csv_path: "./Data.csv"

config app, ResultsAPI.Router, port: 4000

import_config "#{Mix.env()}.exs"
