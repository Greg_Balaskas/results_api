# ResultsAPI
---
HTTP API that serves the data given in a .csv file. 
---

### Requirements
- Install Docker.
- Then `git clone` the project
- Ensure the `Data.csv` is on the root path of the project.
- Ensure `./config/config.exs` is set up as wanted for dev/test env
  or `docker-compose.yml` for prod env.

### How to build documentation
```shell
$ mix do deps.get, docs
```

### How to run tests:
```shell
$ MIX_ENV=test mix do deps.get, test
```

### How to run:
<details>
    <summary>
Docker-compose
    </summary>

Starts three instances of the application that listen to the port `4000`.
Starts a HAProxy instance on top of them, that listens to port `5000`
and load-balances using round robbin (can be configged in `./config/haproxy.cfg`).

1. Build Docker image

Builts an image called `results-api:latest`

```shell
$ docker-compose build
```

2. Start application

Attached to the instances
```shell
$ docker-compose up
```

or

Detached from the instances
```shell
$ docker-compose up -d
```
</details>

<details>
    <summary>
IEX (for debuging)
    </summary>

```shell
$ iex -S mix
```
</details>

---

### How to use API:
<details>

<summary>
GET /div_season_pairs
</summary>

> API that returns all of the available season, match pairs.

```shell
curl --location --request GET "localhost:5000/div_season_pairs"
```
```json
HTTP Status: 200
[
{"season":201617,"div":"E0"},
{"season":201516,"div":"SP1"},
{"season":201617,"div":"SP1"},
{"season":201617,"div":"D1"},
{"season":201516,"div":"SP2"},
{"season":201617,"div":"SP2"}
]
```

</details>
<details>
<summary>
GET /results
</summary>

> API that returns all of the available results, for a div season pair.
  The response is formatted according to the Accept header of the request.
  It can be either "application/json" either "application/protobuf"
  It's mandatory to be set in the call.

```shell
curl --location --header "Accept: application/json" --request GET "localhost:5000/results?div=SP1&season=201617"
```

```shell
curl --location --header "Accept: application/protobuf" --request GET "localhost:5000/results?div=SP1&season=201617"
```

</details>
<details>
<summary>
Response Codes
</summary>

  - 200 - path and/or parametres are valid so results are returned
  - 400 - bad arguments given
  - 404 - trying to access wrong path
  - 406 - when unsupported content-type is asked by the client
  - 500 - any unhandled error raised during serving the requestxs

</details>
<details>
<summary>
JSON Response Format
</summary>
  
```json
{
  "div": "SP1",
  "season": 201617,
  "results": [
    {
      "div": "SP1",
      "season": 201617,
      "date": "21/05/2017",
      "pair": {
        "home_team": "Valencia",
        "away_team": "Villareal"
      },
      "ft_result": {
        "home_goals": 2,
        "away_goals": 2,
        "result": "D"
      },
      "ht_result": {
        "home_goals": 1,
        "away_goals": 2,
        "result": "A"
      }
    },
    ...
  ]
}
```
</details>
<details>
<summary>
Protobuf Response Format
</summary>
Is defined in the './lib/protobuf/response.proto file, which is compiled for elixir,
using the protobuf dependency.
</details>