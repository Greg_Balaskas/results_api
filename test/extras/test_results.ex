defmodule Extras.TestResults do
  @moduledoc false
  @results [
    %ResultsAPI.Models.Match{
      date: "19/08/2016",
      div: :SP1,
      ft_result: %ResultsAPI.Models.Result{
        away_goals: 1,
        home_goals: 2,
        result: "H"
      },
      ht_result: %ResultsAPI.Models.Result{
        away_goals: 0,
        home_goals: 0,
        result: "D"
      },
      pair: %ResultsAPI.Models.Pair{
        away_team: "Eibar",
        home_team: "La Coruna"
      },
      season: 201_617
    },
    %ResultsAPI.Models.Match{
      date: "19/08/2016",
      div: :SP1,
      ft_result: %ResultsAPI.Models.Result{
        away_goals: 1,
        home_goals: 1,
        result: "D"
      },
      ht_result: %ResultsAPI.Models.Result{
        away_goals: 0,
        home_goals: 0,
        result: "D"
      },
      pair: %ResultsAPI.Models.Pair{
        away_team: "Osasuna",
        home_team: "Malaga"
      },
      season: 201_617
    },
    %ResultsAPI.Models.Match{
      date: "20/08/2016",
      div: :SP1,
      ft_result: %ResultsAPI.Models.Result{
        away_goals: 2,
        home_goals: 6,
        result: "H"
      },
      ht_result: %ResultsAPI.Models.Result{
        away_goals: 1,
        home_goals: 3,
        result: "H"
      },
      pair: %ResultsAPI.Models.Pair{
        away_team: "Betis",
        home_team: "Barcelona"
      },
      season: 201_617
    },
    %ResultsAPI.Models.Match{
      date: "20/08/2016",
      div: :SP1,
      ft_result: %ResultsAPI.Models.Result{
        away_goals: 1,
        home_goals: 1,
        result: "D"
      },
      ht_result: %ResultsAPI.Models.Result{
        away_goals: 0,
        home_goals: 0,
        result: "D"
      },
      pair: %ResultsAPI.Models.Pair{
        away_team: "Villarreal",
        home_team: "Granada"
      },
      season: 201_617
    },
    %ResultsAPI.Models.Match{
      date: "20/08/2016",
      div: :SP1,
      ft_result: %ResultsAPI.Models.Result{
        away_goals: 4,
        home_goals: 6,
        result: "H"
      },
      ht_result: %ResultsAPI.Models.Result{
        away_goals: 3,
        home_goals: 3,
        result: "D"
      },
      pair: %ResultsAPI.Models.Pair{
        away_team: "Espanol",
        home_team: "Sevilla"
      },
      season: 201_617
    }
  ]

  @pairs [%{div: :SP1, season: 201_617}]

  def get(:results) do
    @results
  end

  def get(:pairs) do
    @pairs
  end
end
