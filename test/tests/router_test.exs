defmodule Tests.RouterTest do
  use ExUnit.Case, async: true
  use Plug.Test

  alias Extras.TestResults
  alias ResultsAPI.Router

  @opts Router.init([])

  test "wrong path response" do
    conn = conn(:get, "/wrong_path")

    conn = Router.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 404
  end

  test "div season pairs api" do
    conn = conn(:get, "/div_season_pairs")

    conn = Router.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 200
  end

  test "results api wrong accept encoding" do
    conn = conn(:get, "/results?div=SP1&season=201617")

    conn = Router.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 406
  end

  test "results api error" do
    conn = conn(:get, "/results")

    conn = Router.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 400
  end
end
