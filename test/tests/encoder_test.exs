defmodule Tests.EncoderTest do
  use ExUnit.Case, async: true

  alias ResultsAPI.Encoder

  @div :SP1
  @season 201_717
  @models [
    %ResultsAPI.Models.Match{
      date: "20/08/2016",
      div: :SP1,
      ft_result: %ResultsAPI.Models.Result{
        away_goals: 4,
        home_goals: 6,
        result: "H"
      },
      ht_result: %ResultsAPI.Models.Result{
        away_goals: 3,
        home_goals: 3,
        result: "D"
      },
      pair: %ResultsAPI.Models.Pair{
        away_team: "Espanol",
        home_team: "Sevilla"
      },
      season: 201_617
    }
  ]

  test "json encoder" do
    assert Encoder.encode!(:json, @models, @div, @season) == File.read!("./test/extras/test_json.bin")
  end

  test "protobuf encoder" do
    assert Encoder.encode!(:proto, @models, @div, @season) == File.read!("./test/extras/test_proto.bin")
  end
end
