defmodule Tests.Models.Match do
  use ExUnit.Case, async: true

  alias Extras.TestResults
  alias NimbleCSV.RFC4180, as: CSV
  alias ResultsAPI.Models.Match

  @test_csv "./test/extras/Data.csv"

  test "test parsing of rows" do
    test_rows =
      @test_csv
      |> File.read!()
      |> CSV.parse_string()
      |> Enum.map(fn row -> Match.parse_row(row) end)

    assert test_rows == TestResults.get(:results)
  end
end
