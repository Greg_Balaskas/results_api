defmodule Tests.CacheTest do
  use ExUnit.Case, async: true

  alias Extras.TestResults
  alias ResultsAPI.Cache

  @div :SP1
  @season 201_617
  @wrong_div :SP2
  @wrong_season 201_314

  test "test cache pairs fetch" do
    assert Cache.get_div_season_pairs() == TestResults.get(:pairs)
  end

  test "test cache results fetch" do
    assert Cache.get_results_for_div_season(@div, @season) == {:ok, TestResults.get(:results)}

    assert Cache.get_results_for_div_season(@wrong_div, @season) == {:error, :div_not_in_data}

    assert Cache.get_results_for_div_season(@div, @wrong_season) ==
             {:error, :no_results_for_div_season_pair}
  end
end
